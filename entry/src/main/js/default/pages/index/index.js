import Log from './log.js'
import operatorConfig from './operatorConfig.js'
import OperatorCode from './OperatorCode.js'

var getRequestAction = function (requestCode) {
    return {
        bundleName: operatorConfig.OP_BUNDLE_NAME,
        abilityName: operatorConfig.COMMON_ABILITY_NAME,
        abilityType: OperatorCode.ABILITY_TYPE_INTERNAL,
        syncOption: OperatorCode.ACTION_SYNC,
        messageCode: requestCode,
    };
};

export default {
    data: {
        title: "",
        isStart: false,
        bgState: false,
        index: -1, // 当前转动到哪个位置，起点位置
        count: 8, // 总共有多少个位置
        timer: 0, // 每次转动定时器
        speed: 200, // 初始转动速度
        times: 0, // 转动次数
        cycle: 50, // 转动基本次数：即至少需要转动多少次再进入抽奖环节
        list: [
            {
                img: '/common/images/thanks.png', title: '谢谢参与'
            },
            {
                img: '/common/images/bijb.png', title: '笔记本'
            },
            {
                img: '/common/images/baoma.png', title: '宝马一辆'
            },
            {
                img: '/common/images/hw_p50_pocket.png', title: '华为P50'
            },
            {
                img: '/common/images/apple.jpg', title: '苹果一蓝'
            },
            {
                img: '/common/images/qian.png', title: '500红包'
            },
            {
                img: '/common/images/qqan.jpg', title: '冰墩墩'
            },
            {
                img: '/common/images/hua.jpg', title: '鲜花一蓝'
            }
        ],
        prize: -1, // 中奖位置
        prizeItem: {},
        width: 960,
        height: 540,
        bgWidth: 0,
        bgHeight: 0,
        bgPadding: 20,
        itemWidth: 0,
        itemHeight: 0
    },
    onInit() {
        this.setInterval5();
    },
    onShow() {
        // xxx.js
        var rect = this.$element('container').getBoundingClientRect();
        Log.info(`current element position is ${JSON.stringify(rect)}`);
        if (rect.width == 0) {
            this.getDeviceHeight();
        } else {
            this.height = rect.height;
            this.width = rect.width;
            this.bgHeight = (this.width > this.height ? this.height : this.width) - 2 * this.bgPadding;
            this.bgWidth = parseInt(this.bgHeight * 618 / 660);
            this.itemWidth = (this.bgWidth - (6 * this.bgPadding)) / 3;
            this.itemHeight = parseInt(this.itemWidth * 261 / 249);

            Log.info(` bgHeight： ${this.bgHeight}  ;bgWidth： ${this.bgWidth}`);
            Log.info(` itemWidth： ${this.itemWidth}  ;itemHeight： ${this.itemHeight}`);
        }

    },
    changeBgState() {
        this.bgState = !this.bgState
    },

    setInterval5() {
        var This = this;
        setInterval(function () {
            This.changeBgState();
        }, 500)
    },

    startLottery() {
        if (this.isStart) {
            return
        }
        this.isStart = true;
        this.startRoll();
    },

    // 开始转动
    startRoll() {
        this.times += 1 // 转动次数
        this.oneRoll() // 转动过程调用的每一次转动方法，这里是第一次调用初始化
        // 如果当前转动次数达到要求 && 目前转到的位置是中奖位置
        if (this.times > this.cycle + 10 && this.prize === this.index) {
            clearTimeout(this.timer) // 清除转动定时器，停止转动
            this.prize = -1
            this.times = 0
            this.speed = 200
            this.isStart = false;
            var that = this;
            setTimeout(res => {
                that.dialogShow();
            }, 500)
        } else {
            if (this.times < this.cycle) {
                this.speed -= 10 // 加快转动速度
            } else if (this.times === this.cycle) {
                const index = parseInt(Math.random() * 7, 0) || 0; // 随机获得一个中奖位置
                this.prize = index; //中奖位置,可由后台返回
                if (this.prize > 7) {
                    this.prize = 7
                }
                this.prizeItem = this.list[this.prize]
            } else if (this.times > this.cycle + 10 && ((this.prize === 0 && this.index === 7) || this.prize === this.index + 1)) {
                this.speed += 110
            } else {
                this.speed += 20
            }
            if (this.speed < 40) {
                this.speed = 40
            }
            this.timer = setTimeout(this.startRoll, this.speed)
        }
    },
    // 每一次转动
    oneRoll() {
        let index = this.index // 当前转动到哪个位置
        const count = this.count // 总共有多少个位置
        index += 1
        if (index > count - 1) {
            index = 0
        }
        this.index = index
    },

    // 获取屏幕宽高
    async getDeviceHeight() {
        let actionData = {};
        let action = getRequestAction(OperatorCode.GET_DEVICE_HEIGHT);
        action.data = actionData;
        var result = await FeatureAbility.callAbility(action);
        var ret = JSON.parse(result);
        Log.info(` getDeviceHeight ret： ${JSON.stringify(ret)}`);
        this.height = ret.height / 2;
        this.width = ret.width / 2;

        this.bgHeight = parseInt((this.width > this.height ? this.height : this.width) - 2 * this.bgPadding);
        this.bgWidth = parseInt(this.bgHeight * 618 / 660);
        this.itemWidth = parseInt((this.bgWidth - (6 * this.bgPadding)) / 3);
        this.itemHeight = parseInt(this.itemWidth * 261 / 249);

        Log.info(` bgHeight： ${this.bgHeight}  ;bgWidth： ${this.bgWidth}`);
        Log.info(` itemWidth： ${this.itemWidth}  ;itemHeight： ${this.itemHeight}`);
    },
    dialogShow: function () {
        let self = this;
        self.$element('deviceCommonDialog').show();
    }
}
