const logSetting = {
    LOG_DEBUG: true,
    LOG_TAG: 'JS_LuckTag'
}

class Log {
    info(data) {
        if (logSetting.LOG_DEBUG) {
            console.info(logSetting.LOG_TAG + ':' + data)
        }
    }

    error(data) {
        if (logSetting.LOG_DEBUG) {
            console.error(logSetting.LOG_TAG + ':' + data)
        }
    }

    debug(data) {
        if (logSetting.LOG_DEBUG) {
            console.debug(logSetting.LOG_TAG + ':' + data)
        }
    }

    warn(data) {
        if (logSetting.LOG_DEBUG) {
            console.warn(logSetting.LOG_TAG + ':' + data)
        }
    }

    log(data) {
        if (logSetting.LOG_DEBUG) {
            console.log(logSetting.LOG_TAG + ':' + data)
        }
    }
}

var log = new Log()

export default log