const OperatorCode = {
    GET_DEVICE_LIST : 1001 ,
    START_ABILITY: 1002,
    START_GAME: 1003,
    ON_DATA_CHANGE: 1004,
    GET_DEVICE_HEIGHT: 1005,
    SHARE_PICTURE: 1006,

// abilityType: 0-Ability; 1-Internal Ability
    ABILITY_TYPE_EXTERNAL: 0,
    ABILITY_TYPE_INTERNAL: 1,
// syncOption: 0-Sync; 1-Async
    ACTION_SYNC: 0,
    ACTION_ASYNC: 1,
}
export default OperatorCode

