package ohos.pvj.jsluckdemo;

import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        CommonInternalAbility.register(this);
    }


    @Override
    public void onEnd() {
        super.onEnd();
        CommonInternalAbility.unregister();
    }
}
