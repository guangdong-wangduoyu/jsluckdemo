package ohos.pvj.jsluckdemo;

import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class DisplayUtils {

    /**
     * 获取屏幕宽度
     */
    public static int getDisplayWidth(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point);
        return point.getPointXToInt();
    }

    public static int getWindowWidth(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        DisplayAttributes attributes = display.getAttributes();
        return attributes.width;
    }

    /**
     * 获取屏幕高度
     */
    public static int getDisplayHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point);
        return point.getPointYToInt();
    }


}
